
def GITLAB_ACTION = ""
def BRANCH = ""
def DESTINATAIRES = ""

pipeline {
    agent {
        label 'WINDOWS'
    }


    parameters {
        booleanParam(defaultValue: false, description: 'Clean the workspace ?', name: 'CLEAN_WORKSPACE')
        booleanParam(defaultValue: true, description: 'Clean the workspace ?', name: 'CHECKOUT')
        booleanParam(defaultValue: false, description: 'Compile Project ?', name: 'BUILD')
        booleanParam(defaultValue: false, description: 'Run Units Tests ?', name: 'UNIT_TESTS')
        booleanParam(defaultValue: false, description: 'Sonar Analysis ?', name: 'SONAR')
        gitParameter branch: '', branchFilter: '.*', defaultValue: 'origin/master', description: 'Which Branch ?', name: 'BRANCH_', quickFilterEnabled: true, selectedValue: 'NONE', sortMode: 'NONE', tagFilter: '*', type: 'PT_BRANCH_TAG', useRepository: 'https://gitlab.com/ayoubensalem/blogengine.git'
    }


    stages{   

        stage('CleanWS') {
            when {
                expression { params.CLEAN_WORKSPACE   }
            }
            steps {
                script {
                    cleanWs()
                } 
            }
        }  
        stage('1_Checkout') {
            when {
                expression { params.CHECKOUT }
            }
            steps {
                script {
                    BRANCH = env.gitlabSourceBranch ?: params.BRANCH_
                    GITLAB_ACTION = env.gitlabActionType ?: null 

                    if ( GITLAB_ACTION != null ) {
                        if (DESTINATAIRES == "") {
                            DESTINATAIRES = env.gitlabUserEmail
                        } else {
                            DESTINATAIRES = DESTINATAIRES + "," + env.gitlabUserEmail
                        }
                    }


                    checkout([$class: 'GitSCM', branches: [[name: "${BRANCH}"]], 
                        doGenerateSubmoduleConfigurations: false, 
                        extensions: [[$class: 'CleanBeforeCheckout']], 
                        submoduleCfg: [], 
                        userRemoteConfigs: [[url: 'https://gitlab.com/ayoubensalem/blogengine.git']]]
                    )
                    checkout([$class: 'GitSCM', branches: [[name: '*/master']], 
                        doGenerateSubmoduleConfigurations: false, 
                        extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'conf']], 
                        submoduleCfg: [], 
                        userRemoteConfigs: [[credentialsId: '70933069-eddd-48a2-aed6-86d3c59316bf', 
                        url: 'https://github.com/ayoubensalem/conf.git']]])

                    addShortText(text: "BRANCH: ${BRANCH}", background: "#AFCB07", borderColor: "#AFCB09", border: 1, color: "White")
                    println("BRANCH -----> ${BRANCH}")
                    println("GITLAB_ACTION -----> ${GITLAB_ACTION}")

                }
            }
        }   
        stage('2_Build') {
            when {
                expression { params.BUILD || ( GITLAB_ACTION == 'MERGE'  ) || (  GITLAB_ACTION == 'NOTE'  ) }
            }
            steps {
                script {
                    gitlabBuilds(builds: ['3_SONAR']){
                        bat "%workspace%\\conf\\scripts\\build.bat %workspace%\\BlogEngine\\BlogEngine.sln"
                    }
                }
            }
            post {
                failure {
                    script {
                        updateGitlabCommitStatus name: '2_Build', state: 'failed'
                        updateGitlabCommitStatus name: 'MERGE_REQ_PROCESS', state: 'failed'
                    }
                }

                success {
                    script {
                        updateGitlabCommitStatus name: '2_Build', state: 'success'
                        updateGitlabCommitStatus name: 'MERGE_REQ_PROCESS', state: 'pending'

                    }
                }
            }
        }

       stage('3_SONAR') {
           when {
               expression { params.SONAR || ( GITLAB_ACTION == 'MERGE'  ) || (  GITLAB_ACTION == 'NOTE' ) }
           }
            steps {
                script {
                    gitlabBuilds(builds: ['3_SONAR']){
                        bat "%workspace%\\conf\\scripts\\sonar.bat %workspace%\\BlogEngine\\BlogEngine.sln com.company.blogengine BlogEngine 0.0.1 ${BRANCH} %workspace%"
                    }
                    def qualityGate = getQualityGate(getSonarTask(env.WORKSPACE))

                    switch(qualityGate) {
                        case "OK":
                            println("Quality Qate Status : ${qualityGate}")
                            addShortText(text: "Quality Gate : ${qualityGate}", background: "#AFCB07", borderColor: "#AFCB07", border: 1, color: "White")
                            currentBuild.result = 'SUCCESS'
                            break
                        case "WARN":
                            println("Quality Qate Status : ${qualityGate}")
                            addShortText(text: "Quality Gate : ${qualityGate}", background: "#FFAD17", borderColor: "#FFAD17", border: 1, color: "White")
                            currentBuild.result = 'UNSTABLE'
                            break
                        case "ERROR":
                            addShortText(text: "Quality Gate : ${qualityGate}", background: "#D02A2F", borderColor: "#D02A2F", border: 1, color: "White")
                            println("Quality Qate Status : ${qualityGate}")
                            currentBuild.result = 'FAILURE'
                            if ( GITLAB_ACTION == "MERGE" || GITLAB_ACTION == 'NOTE' )
                                addGitLabMRComment "http://67.207.94.110:9000/project/issues?id=com.company.blogengine&resolved=false"
                            break
                        case "NONE":
                            error("No quality gate associated with the analysis. Please check your configuration")
                            break
                    }
                }
            }
            post {
                failure {
                    script {
                        updateGitlabCommitStatus name: '3_SONAR', state: 'failed'
                        updateGitlabCommitStatus name: 'MERGE_REQ_PROCESS', state: 'failed'
                    }
                }

                success {
                    script {
                        updateGitlabCommitStatus name: '3_SONAR', state: 'success'
                        updateGitlabCommitStatus name: 'MERGE_REQ_PROCESS', state: 'pending'

                    }
                }
            }
        }

        
    }
    post {
        always {
            updateGitlabCommitStatus name: 'MERGE_REQ_PROCESS', state: 'success'
        }
        failure {
            sendEmail("Failed", DESTINATAIRES, env.BUILD_URL, env.JOB_NAME , env.BUILD_NUMBER)
        }
        success {
            sendEmail("Success", DESTINATAIRES, env.BUILD_URL, env.JOB_NAME , env.BUILD_NUMBER)
        }
    }
}


def getContent(def props) {

    StringWriter writer = new StringWriter();
    props.store(writer, "")
    return writer.getBuffer().toString()
}


def sendEmail(def status, def recpts, def url, def pipeline_name, def pipeline_number){
    emailext(
            subject: "${pipeline_name} #[${pipeline_number}] ${status} !!",
            //attachLog: true,
            body: """
                Merci de consulter les logs sur Jenkins pour la Pipeline : ${pipeline_name} dans le lien suivant: ${url}
            """,
            to: "${recpts}"

    )
}

def getSonarTask(def workspace){

    Properties taskProperties = new Properties()

    if (fileExists("${workspace}\\report-task.txt")){
        def taskPropertiesFile = readFile "${workspace}\\report-task.txt"
        taskProperties.load(new StringReader(taskPropertiesFile));
    }
    else {
        error("File 'report-task.txt' not found in ${workspace}")
    }

    return taskProperties.getProperty("ceTaskId")
}

def getQualityGate(def ceTaskId){
    def SONAR_URL = "http://67.207.94.110:9000/"
    def analysisId = ""
    def qualityGateStatus = ""

    def taskStatus = "NONE"

    while ( taskStatus == "NONE" || taskStatus == "PENDING" || taskStatus == "IN_PROGRESS" ) {
        println "waiting for sonar results..."
        // sleep(time:3, unit:"SECONDS")
        sh "sleep 3"

        def response = httpRequest customHeaders: [[maskValue: true, name: 'Authorization', value: 'Basic YWRtaW46YWRtaW4=']],
                ignoreSslErrors: true, responseHandle: 'STRING',
                url: "${SONAR_URL}/api/ce/task?id=${ceTaskId}"


        def taskResponseJson = readJSON text: response.content

        if (response.status == 200){
            def taskObject = taskResponseJson["task"]
            taskStatus = taskObject["status"]
            println("Task Status is : ${taskStatus}")
            analysisId = taskObject["analysisId"]
        }
        else if ( response.status == 404 ){
            error("SonarQube seems to be down !!")
        } else {
            error("Not handled ${reponse.status} status code !!")
        }
    }

    if (taskStatus == "FAILED" || taskStatus == "CANCELED"){
        println("===> Compute Engine status : ${taskStatus}")
        error("An error has occured while scanning, Please re-run your sonar analysis")
    } else if ( taskStatus == "SUCCESS" ){
        println("===> Compute Engine status : ${taskStatus}")
        def response = httpRequest customHeaders: [[maskValue: true, name: 'Authorization', value: 'Basic YWRtaW46YWRtaW4=']],
                ignoreSslErrors: true, responseHandle: 'STRING',
                url: "${SONAR_URL}/api/qualitygates/project_status?analysisId=${analysisId}"


        def projectResponseJson = readJSON text: response.content

        if (response.status == 200){
            def projectStatus = projectResponseJson["projectStatus"]
            qualityGateStatus = projectStatus["status"]
            println("===> qualityGateStatus for analysis ${analysisId} is : ${qualityGateStatus}")
        }
        else if ( response.status == 404 ){
            error("The analysis ${analysisId} associated with the task  ${ceTaskId} is not found or does not exist")
        } else {
            error("Not handled ${reponse.status} status code !!")
        }

    }

    return qualityGateStatus // Must be one of those ["OK, WARN, ERROR, NONE"]
}